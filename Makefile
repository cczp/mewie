.POSIX:

CP = cp -f
MKDIR = mkdir -p
RM = rm -f

HEADER = mewie.h

OBJECTS = main.o arg-parser.o lexer.o parser.o ast-runner.o

BIN = mewie

PREFIX = /usr/local

all: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $(BIN) $(OBJECTS)

main.o: main.c $(HEADER)
	$(CC) $(CFLAGS) -c main.c

arg-parser.o: arg-parser.c $(HEADER)
	$(CC) $(CFLAGS) -c arg-parser.c

lexer.o: lexer.c $(HEADER)
	$(CC) $(CFLAGS) -c lexer.c

parser.o: parser.c $(HEADER)
	$(CC) $(CFLAGS) -c parser.c

code-gen.o: ast-runner.c $(HEADER)
	$(CC) $(CFLAGS) -c ast-runner.c

clean:
	$(RM) $(OBJECTS) $(BIN)

install: all
	$(MKDIR) $(PREFIX)/bin
	
	$(CP) $(BIN) $(PREFIX)/bin

uninstall:
	$(RM) $(PREFIX)/bin/$(BIN)
