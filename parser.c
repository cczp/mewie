#include "mewie.h"

enum NyashcaParsingStatus {
	MEWIE_EVERYTHING_IS_FINE,
	MEWIE_OOPS_THERE_IS_BRANCH_AGAIN,
	MEWIE_OOPS_THERE_IS_BRANCH_OTHERWISE,
	MEWIE_OOPS_THERE_IS_BLOCK_END,
	MEWIE_BUMPED_INTO_EOF
};

struct NyashcaNode *mewie_parse_expression(
	bool,
	
	enum NyashcaParsingStatus *
);

struct NyashcaNode *mewie_parse_simple_block(enum NyashcaNodeType);

struct NyashcaNode *mewie_parse_branch(void);

void mewie_print_node_list(
	struct NyashcaNodeList *,
	
	size_t
);

struct NyashcaNode *mewie_get_node(void) {
	struct NyashcaNode *node;
	
	enum NyashcaParsingStatus status;
	
	static bool bumped_into_eof = false;
	
	if (bumped_into_eof) {
		bumped_into_eof = false;
		
		return NULL;
	}
	
	node = mewie_parse_expression(false, &status);
	
	switch (status) {
	case MEWIE_EVERYTHING_IS_FINE:
		return node;
	case MEWIE_OOPS_THERE_IS_BRANCH_AGAIN:
		mewie_die("unexpected branch");
	case MEWIE_OOPS_THERE_IS_BRANCH_OTHERWISE:
		mewie_die("unexpected branch");
	case MEWIE_OOPS_THERE_IS_BLOCK_END:
		mewie_die("unexpected block end");
	case MEWIE_BUMPED_INTO_EOF:
		bumped_into_eof = true;
		
		return node;
	}
}

struct NyashcaNode *mewie_parse_expression(
	bool is_inline,
	
	enum NyashcaParsingStatus *status
) {
	struct NyashcaNode *node;
	
	struct NyashcaNodeList *current_node_list;
	
	struct NyashcaToken *current_token;
	
	if (!is_inline) {
		*status = MEWIE_EVERYTHING_IS_FINE;
	}
	
	if ((node = malloc(sizeof(struct NyashcaNode))) == NULL) {
		mewie_die(strerror(errno));
	}
	
	node->type = MEWIE_NODE_EXPRESSION;
	
	if ((current_node_list = node->value.as_node_list =
		malloc(sizeof(struct NyashcaNodeList))
	) == NULL) {
		mewie_die(strerror(errno));
	}
	
	while (true) {
		current_node_list->value = NULL;
		current_node_list->next = NULL;
		
		current_token = mewie_get_token();
		
		if (current_token == NULL) {
			if (is_inline) {
				mewie_die("unexpected end of file");
			}
			
			*status = MEWIE_BUMPED_INTO_EOF;
			
			return node;
		}
		
		switch (current_token->type) {
		case MEWIE_TOKEN_LINE_BREAK:
			if (is_inline) {
				mewie_die("unexpected line break");
			}
			
			return node;
		case MEWIE_TOKEN_EXPRESSION_END:
			if (is_inline) {
				mewie_die("unexpected semicolon");
			}
			
			return node;
		case MEWIE_TOKEN_INLINE_EXPRESSION_START:
			current_node_list->value = mewie_parse_expression(true, NULL);
			
			break;
		case MEWIE_TOKEN_INLINE_EXPRESSION_END:
			if (is_inline) {
				return node;
			}
			
			mewie_die("unexpected closing parenthesis");
		case MEWIE_TOKEN_QUOTE:
			current_token = mewie_get_token();
			
			if (current_token == NULL) {
				mewie_die("unexpected end of file");
			}
			
			switch (current_token->type) {
			case MEWIE_TOKEN_LINE_BREAK:
				mewie_die("unexpected line break");
			case MEWIE_TOKEN_EXPRESSION_END:
				mewie_die("unexpected semicolon");
			case MEWIE_TOKEN_INLINE_EXPRESSION_START:
				current_node_list->value = mewie_parse_expression(true, NULL);
				
				current_node_list->value->type = MEWIE_NODE_LIST_VALUE;
				
				break;
			case MEWIE_TOKEN_INLINE_EXPRESSION_END:
				mewie_die("unexpected closing parenthesis");
			case MEWIE_TOKEN_QUOTE:
				mewie_die("unexpected quote");
			case MEWIE_TOKEN_INT_VALUE: // FALLTHROUGH
			case MEWIE_TOKEN_FLOAT_VALUE:
				mewie_die("unexpected number");
			case MEWIE_TOKEN_STRING_VALUE:
				mewie_die("unexpected string");
			case MEWIE_TOKEN_SYMBOL_VALUE:
				if ((current_node_list->value =
					malloc(sizeof(struct NyashcaNode))
				) == NULL) {
					mewie_die(strerror(errno));
				}
				
				current_node_list->value->type = MEWIE_NODE_SYMBOL_VALUE;
				
				current_node_list->value->value.as_string = current_token->value.as_string;
				
				break;
			case MEWIE_TOKEN_FUNCTION_START:
				mewie_die("unexpected function start");
			case MEWIE_TOKEN_BRANCH_START: // FALLTHROUGH
			case MEWIE_TOKEN_BRANCH_AGAIN: // FALLTHROUGH
			case MEWIE_TOKEN_BRANCH_OTHERWISE:
				mewie_die("unexpected branch");
			case MEWIE_TOKEN_LOOP_START:
				mewie_die("unexpected loop start");
			case MEWIE_TOKEN_BLOCK_END:
				mewie_die("unexpected block end");
			default:
				mewie_die("internal: invalid token provided");
			}
			
			break;
		case MEWIE_TOKEN_INT_VALUE:
			if ((current_node_list->value =
				malloc(sizeof(struct NyashcaNode))
			) == NULL) {
				mewie_die(strerror(errno));
			}
			
			current_node_list->value->type = MEWIE_NODE_INT_VALUE;
			
			current_node_list->value->value.as_int = current_token->value.as_int;
			
			break;
		case MEWIE_TOKEN_FLOAT_VALUE:
			if ((current_node_list->value =
				malloc(sizeof(struct NyashcaNode))
			) == NULL) {
				mewie_die(strerror(errno));
			}
			
			current_node_list->value->type = MEWIE_NODE_FLOAT_VALUE;
			
			current_node_list->value->value.as_float = current_token->value.as_float;
			
			break;
		case MEWIE_TOKEN_STRING_VALUE:
			if ((current_node_list->value =
				malloc(sizeof(struct NyashcaNode))
			) == NULL) {
				mewie_die(strerror(errno));
			}
			
			current_node_list->value->type = MEWIE_NODE_STRING_VALUE;
			
			current_node_list->value->value.as_string = current_token->value.as_string;
			
			break;
		case MEWIE_TOKEN_SYMBOL_VALUE:
			if ((current_node_list->value =
				malloc(sizeof(struct NyashcaNode))
			) == NULL) {
				mewie_die(strerror(errno));
			}
			
			current_node_list->value->type = MEWIE_NODE_VARIABLE_REFERENCE;
			
			current_node_list->value->value.as_string = current_token->value.as_string;
			
			break;
		case MEWIE_TOKEN_FUNCTION_START:
			current_node_list->value = mewie_parse_simple_block(MEWIE_NODE_FUNCTION_VALUE);
			
			break;
		case MEWIE_TOKEN_BRANCH_START:
			current_node_list->value = mewie_parse_branch();
			
			break;
		case MEWIE_TOKEN_BRANCH_AGAIN:
			if (is_inline) {
				mewie_die("unexpected branch");
			}
			
			*status = MEWIE_OOPS_THERE_IS_BRANCH_AGAIN;
			
			return node;
		case MEWIE_TOKEN_BRANCH_OTHERWISE:
			if (is_inline) {
				mewie_die("unexpected branch");
			}
			
			*status = MEWIE_OOPS_THERE_IS_BRANCH_OTHERWISE;
			
			return node;
		case MEWIE_TOKEN_LOOP_START:
			current_node_list->value = mewie_parse_simple_block(MEWIE_NODE_LOOP);
			
			break;
		case MEWIE_TOKEN_BLOCK_END:
			if (is_inline) {
				mewie_die("unexpected block end");
			}
			
			*status = MEWIE_OOPS_THERE_IS_BLOCK_END;
			
			return node;
		default:
			mewie_die("internal: invalid token provided");
			
			break;
		}
		
		if ((current_node_list = current_node_list->next =
			malloc(sizeof(struct NyashcaNodeList))
		) == NULL) {
			mewie_die(strerror(errno));
		}
	}
}

struct NyashcaNode *mewie_parse_simple_block(
	enum NyashcaNodeType type
){
	struct NyashcaNode *node;
	
	struct NyashcaNodeList *current_node_list;
	
	enum NyashcaParsingStatus status;
	
	if ((node = malloc(sizeof(struct NyashcaNode))) == NULL) {
		mewie_die(strerror(errno));
	}
	
	switch (type) {
	case MEWIE_NODE_FUNCTION_VALUE: // FALLTHROUGH
	case MEWIE_NODE_LOOP:
		break;
	default:
		mewie_die("internal: invalid node type provided");
	}
	
	node->type = type;
	
	if ((current_node_list = node->value.as_node_list =
		malloc(sizeof(struct NyashcaNodeList))
	) == NULL) {
		mewie_die(strerror(errno));
	}
	
	while (true) {
		current_node_list->value = mewie_parse_expression(false,
			&status
		);
		current_node_list->next = NULL;
		
		switch (status) {
		case MEWIE_EVERYTHING_IS_FINE:
			break;
		case MEWIE_OOPS_THERE_IS_BRANCH_AGAIN: // FALLTHROUGH
		case MEWIE_OOPS_THERE_IS_BRANCH_OTHERWISE:
			mewie_die("unexpected branch");
		case MEWIE_OOPS_THERE_IS_BLOCK_END:
			return node;
		case MEWIE_BUMPED_INTO_EOF:
			mewie_die("unexpected end of file");
		default:
			mewie_die("internal: invalid parsing status provided");
		}
		
		if ((current_node_list = current_node_list->next =
			malloc(sizeof(struct NyashcaNodeList))
		) == NULL) {
			mewie_die(strerror(errno));
		}
	}
}

struct NyashcaNode *mewie_parse_branch(void) {
	struct NyashcaNode *node;
	
	struct NyashcaNodeList *current_branch;
	struct NyashcaNodeList *current_node_list;
	
	enum NyashcaParsingStatus status;
	
	bool has_branch_otherwise = false;
	
	if ((node = malloc(sizeof(struct NyashcaNode))) == NULL) {
		mewie_die(strerror(errno));
	}
	
	node->type = MEWIE_NODE_BRANCH;
	
	if ((current_branch = node->value.as_node_list =
		malloc(sizeof(struct NyashcaNodeList))
	) == NULL) {
		mewie_die(strerror(errno));
	}
	
	if ((current_branch->value =
		malloc(sizeof(struct NyashcaNode))
	) == NULL) {
		mewie_die(strerror(errno));
	}
	
	current_branch->next = NULL;
	
	current_branch->value->type = MEWIE_NODE_EXPRESSION;
	
	if ((
		current_node_list =
		current_branch->value->value.as_node_list =
		
		malloc(sizeof(struct NyashcaNodeList))
	) == NULL) {
		mewie_die(strerror(errno));
	}
	
	while (true) {
		current_node_list->value = mewie_parse_expression(false,
			&status
		);
		current_node_list->next = NULL;
		
		switch (status) {
		case MEWIE_EVERYTHING_IS_FINE:
			break;
		case MEWIE_OOPS_THERE_IS_BRANCH_AGAIN: // FALLTHROUGH
		case MEWIE_OOPS_THERE_IS_BRANCH_OTHERWISE:
			if (has_branch_otherwise) {
				mewie_die("unexpected branch");
			}
			
			if (status == MEWIE_OOPS_THERE_IS_BRANCH_OTHERWISE) {
				has_branch_otherwise = true;
			}
			
			if ((current_branch = current_branch->next =
				malloc(sizeof(struct NyashcaNodeList))
			) == NULL) {
				mewie_die(strerror(errno));
			}
			
			if ((current_branch->value =
				malloc(sizeof(struct NyashcaNode))
			) == NULL) {
				mewie_die(strerror(errno));
			}
			
			current_branch->next = NULL;
			
			current_branch->value->type = MEWIE_NODE_EXPRESSION;
			
			if ((
				current_node_list =
				current_branch->value->value.as_node_list =
				
				malloc(sizeof(struct NyashcaNodeList))
			) == NULL) {
				mewie_die(strerror(errno));
			}
			
			continue;
		case MEWIE_OOPS_THERE_IS_BLOCK_END:
			return node;
		case MEWIE_BUMPED_INTO_EOF:
			mewie_die("unexpected end of file");
		default:
			mewie_die("internal: invalid parsing status provided");
		}
		
		if ((current_node_list = current_node_list->next =
			malloc(sizeof(struct NyashcaNodeList))
		) == NULL) {
			mewie_die(strerror(errno));
		}
	}
}

void mewie_print_node(struct NyashcaNode *node, size_t indent_level) {
	for (size_t i = 0; i < indent_level; i++) {
		printf("\t");
	}
	
	switch (node->type) {
	case MEWIE_NODE_EXPRESSION:
		printf("Expression:\n");
		
		mewie_print_node_list(node->value.as_node_list, indent_level);
		
		break;
	case MEWIE_NODE_INT_VALUE:
		printf("Int value: %" PRId64 "\n", node->value.as_int);
		
		break;
	case MEWIE_NODE_FLOAT_VALUE:
		printf("Float value: %lf\n", node->value.as_float);
		
		break;
	case MEWIE_NODE_STRING_VALUE:
		printf("String value: %s\n", node->value.as_string.value);
		
		break;
	case MEWIE_NODE_VARIABLE_REFERENCE:
		printf("Variable reference: %s\n", node->value.as_string.value);
		
		break;
	case MEWIE_NODE_SYMBOL_VALUE:
		printf("Symbol value: %s\n", node->value.as_string.value);
		
		break;
	case MEWIE_NODE_LIST_VALUE:
		printf("List value:\n");
		
		mewie_print_node_list(node->value.as_node_list, indent_level);
		
		break;
	case MEWIE_NODE_FUNCTION_VALUE:
		printf("Function value:\n");
		
		mewie_print_node_list(node->value.as_node_list, indent_level);
		
		break;
	case MEWIE_NODE_BRANCH:
		printf("Branch:\n");
		
		mewie_print_node_list(node->value.as_node_list, indent_level);
		
		break;
	case MEWIE_NODE_LOOP:
		printf("Loop:\n");
		
		mewie_print_node_list(node->value.as_node_list, indent_level);
		
		break;
	default:
		printf("Invalid node\n");
		
		break;
	}
}

void mewie_print_node_list(
	struct NyashcaNodeList *node_list,
	
	size_t indent_level
) {
	do {
		if (node_list->value == NULL) {
			break;
		}
		
		mewie_print_node(node_list->value, indent_level + 1);
	} while ((node_list = node_list->next) != NULL);
}
